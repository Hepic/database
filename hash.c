#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "BF.h"
#include "hash.h"

#define min(a, b) a < b ? a : b
#define max(a, b) a > b ? a : b 

const int MAX_RECORDS = (BLOCK_SIZE - 4*sizeof(int)) / sizeof(Record);


int HT_CreateIndex(char *fileName, char attrType, char* attrName, int attrLength, int buckets)
{
    int i, fileDesc, pos, seg_num;
    char type;
    void *block; 
    HT_info *info = (HT_info*)malloc(sizeof(HT_info));
    
    if(BF_CreateFile(fileName) < 0)
        return -1;
    
    if((fileDesc = BF_OpenFile(fileName)) < 0)
        return -1;
    
    info->fileDesc = fileDesc;
    info->attrType = attrType;
    info->attrLength = attrLength;
    info->attrName = (char*)malloc(info->attrLength*sizeof(char));
    strcpy(info->attrName, attrName);
    info->numBuckets = buckets;
   
    if(BF_AllocateBlock(fileDesc) < 0)
        return -1;
            
    pos = BF_GetBlockCounter(fileDesc) - 1;

    if(BF_ReadBlock(fileDesc, pos, &block) < 0)
        return -1;
        
    memset(block, 0, BLOCK_SIZE);
         
    type = 's';
    memcpy(block, &type, sizeof(char));
    memcpy(block+sizeof(char), info, sizeof(HT_info));
    
    if(BF_WriteBlock(fileDesc, pos) < 0)
        return -1;

    seg_num = ((buckets-1)*sizeof(int)) / BLOCK_SIZE; // number of blocks that keeps buckets
    
    for(i=0; i<=seg_num; ++i) // create blocks that contains buckets/hashtable
    {
        if(BF_AllocateBlock(fileDesc) < 0)
            return -1;

        pos = BF_GetBlockCounter(fileDesc) - 1;

        if(BF_ReadBlock(fileDesc, pos, &block) < 0)
            return -1;

        memset(block, 0, BLOCK_SIZE);
    
        if(BF_WriteBlock(fileDesc, pos) < 0)
            return -1;
        
        if(BF_ReadBlock(fileDesc, 0, &block) < 0)
            return -1;
        
        memcpy(block+sizeof(char)+sizeof(HT_info)+sizeof(int)*i, &pos, sizeof(int));
    }

    return 0;
}


HT_info* HT_OpenIndex(char *fileName)
{
    int fileDesc;
    void *block;
    HT_info *info = (HT_info*)malloc(sizeof(HT_info));

    if((fileDesc = BF_OpenFile(fileName)) < 0)
        return NULL;
        
    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
        return NULL;
    
    memcpy(info, block+sizeof(char), sizeof(HT_info));
    
    return info;
}


int HT_CloseIndex( HT_info* header_info )
{
    int fileDesc = header_info->fileDesc;
    
    if(BF_CloseFile(fileDesc) < 0)
        return -1; 
    
    return 0;
}


int HT_InsertEntry(HT_info header_info, Record record)
{
    int bucket, block_pos, pos, block_size, seg_num, par, offset, val, i;
    int fileDesc = header_info.fileDesc;
    int buckets_number = header_info.numBuckets;
    char *key = header_info.attrName;
    char str_to_hash[30];
    char attrType = header_info.attrType;
    char type;
    void *block;
    
    if(attrType == 'i')
        bucket = record.id % buckets_number;
    
    else if(attrType == 'c')
    {
        bucket = 0;

        if(strcmp(key, "name") == 0)
            strcpy(str_to_hash, record.name);
        
        else if(strcmp(key, "surname") == 0)
            strcpy(str_to_hash, record.surname);

        else if(strcmp(key, "city") == 0)
            strcpy(str_to_hash, record.city);

        for(i=0; i<strlen(str_to_hash); ++i)
            bucket = (bucket*257 + (int)str_to_hash[i]) % buckets_number;
    }
    
    seg_num = (bucket*sizeof(int)) / BLOCK_SIZE; // number of segment that contains the bucket
    offset = (bucket*sizeof(int)) % BLOCK_SIZE; // position where bucket is located in the segment
    
    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
        return -1;

    memcpy(&block_pos, block+sizeof(char)+sizeof(HT_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve block that contains the bucket
    
    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
        return -1;

    memcpy(&block_pos, block+offset, sizeof(int)); // retrieve block that is connected with the bucket
    
    if(block_pos == 0) // create and initialize first block of bucket
    {
        if(BF_AllocateBlock(fileDesc) < 0)
            return -1;
        
        pos = BF_GetBlockCounter(fileDesc) - 1;
        memcpy(block+offset, &pos, sizeof(int));
            
        if(BF_WriteBlock(fileDesc, block_pos) < 0)
            return -1;
        
        if(BF_ReadBlock(fileDesc, pos, &block) < 0)
            return -1;

        memset(block, 0, BLOCK_SIZE);

        type = 'p';
        memcpy(block+sizeof(int), &type, sizeof(char));

        memcpy(block+sizeof(int)+sizeof(char), &pos, sizeof(int)); // parent of that block

        val = -1;
        memcpy(block+2*sizeof(int)+sizeof(char), &val, sizeof(int)); // next block of that block
        
        memcpy(block+3*sizeof(int)+sizeof(char), &pos, sizeof(int)); // last block of that block
        
        if(BF_WriteBlock(fileDesc, pos) < 0)
            return -1;

        block_pos = pos;
    }
    
    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0) // read first block of the bucket
        return -1;
    
    memcpy(&block_pos, block+3*sizeof(int)+sizeof(char), sizeof(int));
    
    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0) // read last block of bucket
        return -1;
    
    memcpy(&block_size, block, sizeof(int));
    memcpy(&type, block+sizeof(int), sizeof(char));
    
    if(block_size == MAX_RECORDS)
    {
        if(BF_AllocateBlock(fileDesc) < 0)
            return -1;
        
        pos = BF_GetBlockCounter(fileDesc) - 1;
        memcpy(block+2*sizeof(int)+sizeof(char), &pos, sizeof(int));
        
        if(BF_WriteBlock(fileDesc, block_pos) < 0)
            return -1;

        memcpy(&par, block+sizeof(int)+sizeof(char), sizeof(int));
        
        if(BF_ReadBlock(fileDesc, par, &block) < 0)
            return -1;

        memcpy(block+3*sizeof(int)+sizeof(char), &pos, sizeof(int));

        if(BF_WriteBlock(fileDesc, par) < 0)
            return -1;

        if(BF_ReadBlock(fileDesc, pos, &block) < 0)
            return -1;

        memset(block, 0, BLOCK_SIZE);
        block_size = 0;
        block_pos = pos;

        type = 'c';
        memcpy(block+sizeof(int), &type, sizeof(char));

        memcpy(block+sizeof(int)+sizeof(char), &par, sizeof(int)); // parent of that block

        val = -1;
        memcpy(block+2*sizeof(int)+sizeof(char), &val, sizeof(int)); // next block of that block
    }
    
    ++block_size;
    memcpy(block, &block_size, sizeof(int));
    
    if(type == 'p')
        offset = 4*sizeof(int) + sizeof(char) + sizeof(Record)*(block_size-1);
    
    else if(type == 'c')
        offset = 3*sizeof(int) + sizeof(char) + sizeof(Record)*(block_size-1);
    
    memcpy(block+offset, &record, sizeof(Record));
    
    if(BF_WriteBlock(fileDesc, block_pos) < 0)
        return -1;
    
    return 0;
}


int HT_GetAllEntries(HT_info header_info, void *value)
{
    int bucket, block_pos, block_size, i, offset, seg_num, counter = 0;
    int fileDesc = header_info.fileDesc;
    int buckets_number = header_info.numBuckets;
    char *key = header_info.attrName;
    char str_to_hash[30];
    char attrType = header_info.attrType; 
    char type;
    void *block;
    Record record;
    
    if(attrType == 'i')
        bucket = *((int*)value) % buckets_number;
    
    else if(attrType == 'c')
    {
        bucket = 0;
        strcpy(str_to_hash, (char*)value);
        
        for(i=0; i<strlen(str_to_hash); ++i)
            bucket = (bucket*257 + (int)str_to_hash[i]) % buckets_number;
    }
    
    seg_num = (bucket*sizeof(int)) / BLOCK_SIZE; // number of segment that contains the bucket
    offset = (bucket*sizeof(int)) % BLOCK_SIZE; // position where the bucket is located in the segment
     
    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
        return -1;

    memcpy(&block_pos, block+sizeof(char)+sizeof(HT_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve block that contains the bucket
    
    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
        return -1;

    memcpy(&block_pos, block+offset, sizeof(int)); // retrieve block that is connected with the bucket
    
    if(!block)
        return 0;
    
    while(1)
    {
        if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
            return -1;
        
        ++counter;

        memcpy(&block_size, block, sizeof(int));
        memcpy(&type, block+sizeof(int), sizeof(char));
        
        for(i=0; i<block_size; ++i)
        {
            if(type == 'p')
                offset = 4*sizeof(int) + sizeof(char) + sizeof(Record)*i;
             
            else if(type == 'c')
                offset = 3*sizeof(int) + sizeof(char) + sizeof(Record)*i;
                
            memcpy(&record, block+offset, sizeof(Record));
            
            if(strcmp(key, "id") == 0  &&  *((int*)value) == record.id)
                printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
            
            else if(strcmp(key, "name") == 0  &&  strcmp((char*)value, record.name) == 0)
                printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
            
            else if(strcmp(key, "surname") == 0  &&  strcmp((char*)value, record.surname) == 0)
                printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
        
            else if(strcmp(key, "city") == 0  &&  strcmp((char*)value, record.city) == 0)
                printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
        }

        if(block_size == MAX_RECORDS)
        {
            memcpy(&block_pos, block+2*sizeof(int)+sizeof(char), sizeof(int));
        
            if(block_pos == -1)
                break;
        }
        else
            break;
    }

    printf("Number of blocks that we passed: %d\n", counter);

    return 0;
}


int HashStatistics(char* filename)
{
    // Query A
    int fileDesc, buckets_number, val, block_record_number;
    HT_info *info;

    info = HT_OpenIndex(filename);
    fileDesc = info->fileDesc;
    buckets_number = info->numBuckets;
    
    val = ((buckets_number-1)*sizeof(int)) / BLOCK_SIZE;
    block_record_number = BF_GetBlockCounter(fileDesc)-val-2;

    printf("Number of all blocks in the block file: %d\n", BF_GetBlockCounter(fileDesc));
    printf("Number of blocks that are for writing: %d\n", block_record_number);

    // Query B
    int maxim = -1, minim = -1, block_size, i, counter, block_pos, all_records = 0;
    int seg_num, offset;
    void *block;
    
    for(i=0; i<buckets_number; i++)
    {
        seg_num = (i*sizeof(int)) / BLOCK_SIZE; // number of segment that contains ith bucket
        offset = (i*sizeof(int)) % BLOCK_SIZE; // position where the ith bucket is located in the segment

        if(BF_ReadBlock(fileDesc, 0, &block) < 0)
            return -1;

        memcpy(&block_pos, block+sizeof(char)+sizeof(HT_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve block number where contains the ith bucket

        if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
            return -1;

        memcpy(&block_pos, block+offset, sizeof(int)); // retrieve block that is connected with the ith bucket
        counter = 0;
        
        if(!block_pos)
            continue;

        while(1)
        {
            if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
                return -1;
        
            memcpy(&block_size, block, sizeof(int));
            counter += block_size;
            all_records += block_size;

            memcpy(&block_pos, block+2*sizeof(int)+sizeof(char), sizeof(int)); // move to next overflow
            
            if(block_pos == -1)
                break;
        }
        
        minim = (minim == -1 ? counter : min(minim, counter));
        maxim = max(maxim, counter);        
    }

    printf("Minimum records in a bucket: %d\n", minim);
    printf("Maximum records in a bucket: %d\n", maxim);
    printf("Average records in a bucket: %.2f\n", (double)all_records/(double)buckets_number);
    
    // Query C
    printf("Average blocks in a bucket: %.2f\n", (double)block_record_number/(double)buckets_number);

    // Query D
    int bucket_overflow = 0;
    
    for(i=0; i<buckets_number; i++)
    {
        seg_num = (i*sizeof(int)) / BLOCK_SIZE; // number of segment that contains ith bucket
        offset = (i*sizeof(int)) % BLOCK_SIZE; // position where the ith bucket is located in the segment

        if(BF_ReadBlock(fileDesc, 0, &block) < 0)
            return -1;

        memcpy(&block_pos, block+sizeof(char)+sizeof(HT_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve block number where contains the ith bucket

        if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
            return -1;

        memcpy(&block_pos, block+offset, sizeof(int)); // retrieve block that is connected with the ith bucket
        
        if(!block_pos)
            continue;

        if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
            return -1;

        memcpy(&block_pos, block+2*sizeof(int)+sizeof(char), sizeof(int)); // move to next overflow
        
        if(block_pos == -1)
            continue;

        counter = 0;
        ++bucket_overflow;

        while(1)
        {
            if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
                return -1;
        
            ++counter;
            memcpy(&block_pos, block+2*sizeof(int)+sizeof(char), sizeof(int)); // move to next overflow
            
            if(block_pos == -1)
                break;
        }

        printf("%dth bucket has %d overflow blocks\n", i, counter);
    }
    
    printf("Number of buckets that have overflow blocks: %d\n", bucket_overflow);
    return 0;
}

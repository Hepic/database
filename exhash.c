#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "BF.h"
#include "exhash.h"

#define min(a, b) a < b ? a : b
#define max(a, b) a > b ? a : b
#define NUM 17
#define MOD 15485867

const int MAX_RECORDS = (BLOCK_SIZE - 2*sizeof(int)) / sizeof(Record);


int EH_CreateIndex(char *fileName, char* attrName, char attrType, int attrLength, int depth)
{
    int fileDesc, i, val, pos, block_pos, offset, seg_num;
    char type = 'e';
    void *block;
    EH_info *info = (EH_info*)malloc(sizeof(EH_info));

    if(BF_CreateFile(fileName) < 0)
        return -1;

    if((fileDesc = BF_OpenFile(fileName)) < 0)
        return -1;
    
    info->fileDesc = fileDesc;
    info->attrType = attrType;
    info->attrLength = attrLength;
    info->attrName = (char*)malloc(info->attrLength*sizeof(char));
    strcpy(info->attrName, attrName);
    info->depth = depth;
    
    if(BF_AllocateBlock(fileDesc) < 0)
        return -1;

    pos = BF_GetBlockCounter(fileDesc) - 1;

    if(BF_ReadBlock(fileDesc, pos, &block) < 0)
        return -1;

    memset(block, 0, BLOCK_SIZE);
    
    memcpy(block, &type, sizeof(char));
    memcpy(block+sizeof(char), info, sizeof(EH_info));

    if(BF_WriteBlock(fileDesc, pos) < 0)
        return -1;
   
    val = (1<<depth); // number of buckets
    val = ((val-1)*sizeof(int)) / (double)BLOCK_SIZE; // number of blocks/segments that keeps buckets
    
    for(i=0; i<=val; ++i) // create blocks that contain buckets/hashtable
    {
        if(BF_AllocateBlock(fileDesc) < 0)
            return -1;
        
        pos = BF_GetBlockCounter(fileDesc) - 1;

        if(BF_ReadBlock(fileDesc, pos, &block) < 0)
            return -1;

        memset(block, 0, BLOCK_SIZE);
        
        if(BF_WriteBlock(fileDesc, pos) < 0)
            return -1;

        if(BF_ReadBlock(fileDesc, 0, &block) < 0)
            return -1;
        
        memcpy(block+sizeof(char)+sizeof(EH_info)+sizeof(int)*i, &pos, sizeof(int));
        
        if(BF_WriteBlock(fileDesc, 0) < 0)
            return -1;
    }

    for(i=0; i<(1<<depth); ++i) // create buckets and blocks
    {
        if(BF_AllocateBlock(fileDesc) < 0)
            return -1;
        
        pos = BF_GetBlockCounter(fileDesc) - 1;

        if(BF_ReadBlock(fileDesc, pos, &block) < 0)
            return -1;

        memset(block, 0, BLOCK_SIZE);
        memcpy(block+sizeof(int), &depth, sizeof(int)); // set local depth 
        
        if(BF_WriteBlock(fileDesc, pos) < 0)
            return -1;

        if(BF_ReadBlock(fileDesc, 0, &block) < 0)
            return -1;
        
        seg_num = (i*sizeof(int)) / BLOCK_SIZE; // number of segment that contains the ith bucket
        offset = (i*sizeof(int)) % BLOCK_SIZE; // position where the ith bucket is located in the segment
        
        memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve block number that contains the ith bucket
        
        if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
            return -1;
        
        memcpy(block+offset, &pos, sizeof(int)); // connect bucket with the block

        if(BF_WriteBlock(fileDesc, block_pos) < 0)
            return -1;
    }

    return 0; 
}


EH_info* EH_OpenIndex(char *fileName)
{
    int fileDesc;
    void *block;
    EH_info *info = (EH_info*)malloc(sizeof(EH_info));

    if((fileDesc = BF_OpenFile(fileName)) < 0)
        return NULL;

    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
        return NULL;
    
    memcpy(info, block+sizeof(char), sizeof(EH_info));
    
    return info;
} 


int EH_CloseIndex(EH_info* header_info)
{
    int fileDesc = header_info->fileDesc;

    if(BF_CloseFile(fileDesc) < 0)
        return -1;
    
    return 0;
}


int EH_InsertEntry(EH_info* header_info, Record record)
{
    int bucket, keep_bucket, block_pos, block_size, i, j, pos, offset, block_problem;
    int val, hash, seg_num, curr_bucket, local_depth, stop, doubles, len, keep_len;
    int fileDesc = header_info->fileDesc;
    int depth = header_info->depth;
    int team[100];
    char attrType = header_info->attrType;
    char *key = header_info->attrName;
    char str_to_hash[30];
    void *block;
    Record keep_records[MAX_RECORDS+1];
    
    if(attrType == 'i')
        hash = record.id % MOD;
    
    else if(attrType == 'c')
    {
        hash = 0;

        if(strcmp(key, "name") == 0)
            strcpy(str_to_hash, record.name);
        
        if(strcmp(key, "surname") == 0)
            strcpy(str_to_hash, record.surname);

        if(strcmp(key, "city") == 0)
            strcpy(str_to_hash, record.city);
        
        for(i=0; i<strlen(str_to_hash); ++i)
            hash = (hash*NUM + (int)str_to_hash[i]) % MOD;
    }

    bucket = 0;

    for(i=0; i<depth; ++i)
        bucket += (1<<i) & hash;
     
    keep_bucket = bucket;
    
    seg_num = (bucket*sizeof(int)) / (double)BLOCK_SIZE; // number of segment that keeps the bucket
    offset = (bucket*sizeof(int)) % BLOCK_SIZE; // position where the bucket is located in the segment
    
    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
        return -1;

    memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve the block number that contains the bucket
    
    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
        return -1;
    
    memcpy(&block_pos, block+offset, sizeof(int)); // retrieve the block that is connected with the bucket
    
    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
        return -1;
    
    memcpy(&block_size, block, sizeof(int));
    memcpy(&local_depth, block+sizeof(int), sizeof(int));
    
    if(block_size == MAX_RECORDS)
    {
        block_problem = block_pos;
        doubles = 0;

        if(BF_ReadBlock(fileDesc, block_problem, &block) < 0)
            return -1;
         
        for(i=0; i<MAX_RECORDS; ++i)
            memcpy(keep_records+i, block+2*sizeof(int)+sizeof(Record)*i, sizeof(Record));

        keep_records[MAX_RECORDS] = record;
        
        if(local_depth == depth) // doubling the hashtable
        {
            stop = 0;
            doubles = 1;

            while(!stop) // continue doubling the hashtable until the records fits in the hashtable
            {
                if((1<<depth)*sizeof(int) > BLOCK_SIZE/2) // if bucket/hashtable doesnt fit in one block
                {
                    val = ((1<<depth)*sizeof(int)) / BLOCK_SIZE;

                    for(i=0; i<val; ++i) // create more blocks/segments
                    {
                        if(BF_AllocateBlock(fileDesc) < 0)
                            return -1;

                        pos = BF_GetBlockCounter(fileDesc) - 1;
                        
                        if(BF_ReadBlock(fileDesc, pos, &block) < 0)
                            return -1;
                        
                        memset(block, 0, BLOCK_SIZE);

                        if(BF_WriteBlock(fileDesc, pos) < 0)
                            return -1;

                        if(BF_ReadBlock(fileDesc, 0, &block) < 0)
                            return -1;
                        
                        memcpy(block+sizeof(char)+sizeof(EH_info)+sizeof(int)*(val+i), &pos, sizeof(int));
                        
                        if(BF_WriteBlock(fileDesc, 0) < 0)
                            return -1;
                    }
                }
                
                for(i=0; i<(1<<depth); ++i) // copy
                {
                    seg_num = (i*sizeof(int)) / (double)BLOCK_SIZE; // number of segment that keeps the ith bucket
                    offset = (i*sizeof(int)) % BLOCK_SIZE; // position where the ith bucket is located in that segment
                
                    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
                        return -1;

                    memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve block where the ith bucket is located
                    
                    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
                        return -1;
                
                    memcpy(&val, block+offset, sizeof(int)); // retrieve block that is connected with the ith bucket
                
                    seg_num = (((1<<depth)+i)*sizeof(int)) / (double)BLOCK_SIZE;
                    offset = (((1<<depth)+i)*sizeof(int)) % BLOCK_SIZE; 
                    
                    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
                        return -1;
                    
                    memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int));

                    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
                        return -1;

                    memcpy(block+offset, &val, sizeof(int)); // connect new bucket with the proper block
                
                    if(BF_WriteBlock(fileDesc, block_pos) < 0)
                        return -1;
                }
                
                header_info->depth = ++depth; 
                
                if(BF_ReadBlock(fileDesc, 0, &block) < 0)
                    return -1;

                memcpy(block+sizeof(char), header_info, sizeof(EH_info)); // update depth in the block file

                if(BF_WriteBlock(fileDesc, 0) < 0)
                    return -1;

                bucket = keep_bucket + (1<<(depth-1));
                
                for(i=0; i<MAX_RECORDS+1; ++i)
                {
                    if(attrType == 'i')
                        hash = keep_records[i].id % MOD;

                    else if(attrType == 'c')
                    {
                        hash = 0;

                        if(strcmp(key, "name") == 0)
                            strcpy(str_to_hash, keep_records[i].name);
                        
                        else if(strcmp(key, "surname") == 0)
                            strcpy(str_to_hash, keep_records[i].surname);

                        else if(strcmp(key, "city") == 0)
                            strcpy(str_to_hash, keep_records[i].city);
                        
                        for(j=0; j<strlen(str_to_hash); ++j)
                            hash = (hash*NUM + (int)str_to_hash[j]) % MOD;
                    }
                    
                    curr_bucket = 0;
                    
                    for(j=0; j<depth; ++j)
                        curr_bucket += (1<<j) & hash;
                    
                    if(curr_bucket == bucket) // if something fits in the new bucket then we can stop doubling
                    {
                        stop = 1;
                        break;
                    }
                }
            }
        }
        
        stop = 0;

        while(!stop) // splits until all records fit
        {
            if(BF_AllocateBlock(fileDesc) < 0)
                return -1;

            pos = BF_GetBlockCounter(fileDesc) - 1;
            
            if(BF_ReadBlock(fileDesc, pos, &block) < 0)
                return -1;
            
            memset(block, 0, BLOCK_SIZE);
            ++local_depth;

            if(doubles)
                memcpy(block+sizeof(int), &depth, sizeof(int)); // set local depth
            else
                memcpy(block+sizeof(int), &local_depth, sizeof(int));

            if(BF_WriteBlock(fileDesc, pos) < 0)
                return -1;

            len = 0;
            team[len++] = bucket;
            
            if(!doubles)
            {
                for(i=local_depth; i<depth; ++i)
                {
                    keep_len = len;
                    
                    for(j=0; j<keep_len; ++j)
                        team[len++] = team[j] ^ (1<<i);
                }
            }
            
            for(i=0; i<len; ++i)
            {
                seg_num = (team[i]*sizeof(int)) / (double)BLOCK_SIZE; // number of segment that keeps the bucket
                offset = (team[i]*sizeof(int)) % BLOCK_SIZE; // position where the bucket is located in that segment
                
                if(BF_ReadBlock(fileDesc, 0, &block) < 0)
                    return -1;

                memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int)); // retrive block number where the bucket is located

                if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
                    return -1;
                
                memcpy(&val, block+offset, sizeof(int)); // retrieve current block of that bucket

                if(val == block_problem) // we assign to the bucket the new block only if before it was connected with the block_problem
                    memcpy(block+offset, &pos, sizeof(int)); // connect bucket with block
                else
                    team[i] = -1;
            }

            for(i=0; i<MAX_RECORDS+1; ++i)
            {
                if(attrType == 'i')
                    hash = keep_records[i].id % MOD;

                else if(attrType == 'c')
                {
                    hash = 0;
                    
                    if(strcmp(key, "name") == 0)
                        strcpy(str_to_hash, keep_records[i].name);
                    
                    else if(strcmp(key, "surname") == 0)
                        strcpy(str_to_hash, keep_records[i].surname);

                    else if(strcmp(key, "city") == 0)
                        strcpy(str_to_hash, keep_records[i].city);
                    
                    for(j=0; j<strlen(str_to_hash); ++j)
                        hash = (hash*NUM + (int)str_to_hash[j]) % MOD;
                }
                
                curr_bucket = 0;
                
                for(j=0; j<local_depth; ++j)
                    curr_bucket += (1<<j) & hash;
               
                for(j=0; j<len; ++j)
                {
                    if(team[j] != -1  &&  curr_bucket != team[j])
                    {
                        stop = 1;
                        break;
                    }
                }
                
                if(stop)
                    break;
            }

            if(BF_ReadBlock(fileDesc, block_problem, &block) < 0)
                return -1;
            
            memset(block, 0, BLOCK_SIZE); 
            memcpy(block+sizeof(int), &local_depth, sizeof(int)); // update local depth
            
            if(BF_WriteBlock(fileDesc, block_problem) < 0)
                return -1;
            
            block_problem = pos;
        }

        for(i=0; i<MAX_RECORDS+1; ++i)
        {
            if(attrType == 'i')
                hash = keep_records[i].id % MOD;

            else if(attrType == 'c')
            {
                hash = 0;
                
                if(strcmp(key, "name") == 0)
                    strcpy(str_to_hash, keep_records[i].name);
                
                else if(strcmp(key, "surname") == 0)
                    strcpy(str_to_hash, keep_records[i].surname);

                else if(strcmp(key, "city") == 0)
                    strcpy(str_to_hash, keep_records[i].city);
                
                for(j=0; j<strlen(str_to_hash); ++j)
                    hash = (hash*NUM + (int)str_to_hash[j]) % MOD;
            }
            
            curr_bucket = 0;
            
            for(j=0; j<depth; ++j)
                curr_bucket += (1<<j) & hash;
                
            seg_num = (curr_bucket*sizeof(int)) / (double)BLOCK_SIZE; // number of segment that keeps the bucket
            offset = (curr_bucket*sizeof(int)) % BLOCK_SIZE; // position where the bucket is located in that segment
            
            if(BF_ReadBlock(fileDesc, 0, &block) < 0)
                return -1;

            memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int)); // retrive block number where the bucket is located

            if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
                return -1;

            memcpy(&block_pos, block+offset, sizeof(int));
            
            if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
                return -1;
            
            memcpy(&block_size, block, sizeof(int));
                
            ++block_size;
            memcpy(block, &block_size, sizeof(int));
            memcpy(block+2*sizeof(int)+sizeof(Record)*(block_size-1), keep_records+i, sizeof(Record)); 
            
            if(BF_WriteBlock(fileDesc, block_pos) < 0)
                return -1;
        }

        return 0;
    }

    ++block_size;
    memcpy(block, &block_size, sizeof(int));
    memcpy(block+2*sizeof(int)+sizeof(Record)*(block_size-1), &record, sizeof(Record));
        
    if(BF_WriteBlock(fileDesc, block_pos) < 0)
        return -1;

    return 0;
}


int EH_GetAllEntries(EH_info header_info, void *value)
{
    int bucket, seg_num, offset, block_pos, i, block_size, hash;
    int fileDesc = header_info.fileDesc;
    int depth = header_info.depth;
    char *key = header_info.attrName;
    char str_to_hash[30];
    char attrType = header_info.attrType;
    void *block;
    Record record;

    if(attrType == 'i')
        hash = *((int*)value) % MOD;
    
    else if(attrType == 'c')
    {
        hash = 0;
        strcpy(str_to_hash, (char*)value);
        
        for(i=0; i<strlen(str_to_hash); ++i)
            hash = (hash*NUM + (int)str_to_hash[i]) % MOD;
    }

    bucket = 0;

    for(i=0; i<depth; ++i)
        bucket += (1<<i) & hash;
    
    seg_num = (bucket*sizeof(int)) / BLOCK_SIZE; // number of segment where the bucket is located
    offset = (bucket*sizeof(int)) % BLOCK_SIZE; // position where the bucket is located in the segment

    if(BF_ReadBlock(fileDesc, 0, &block) < 0)
        return -1;

    memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve the block number where the bucket is located

    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
        return -1;

    memcpy(&block_pos, block+offset, sizeof(int)); // retrieve the block that is connected with the bucket

    if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
        return -1;
    
    memcpy(&block_size, block, sizeof(int));
    
    for(i=0; i<block_size; ++i)
    {
        memcpy(&record, block+2*sizeof(int)+sizeof(Record)*i, sizeof(Record));
        
        if(strcmp(key, "id") == 0  &&  *((int*)value) == record.id)
            printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
        
        else if(strcmp(key, "name") == 0  &&  strcmp((char*)value, record.name) == 0)
            printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
        
        else if(strcmp(key, "surname") == 0  &&  strcmp((char*)value, record.surname) == 0)
            printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
    
        else if(strcmp(key, "city") == 0  &&  strcmp((char*)value, record.city) == 0)
            printf("id = %d, name = %s, surname = %s, city = %s\n", record.id, record.name, record.surname, record.city);
    }
    
    return 0;
}


int HashStatistics(char* filename)
{

    // Query A
    int fileDesc, depth, val;
    EH_info *info;

    info = EH_OpenIndex(filename);
    fileDesc = info->fileDesc;
    depth = info->depth;
    
    val = (((1<<depth)-1)*sizeof(int)) / BLOCK_SIZE;

    printf("Number of all blocks in the block file: %d\n", BF_GetBlockCounter(fileDesc));
    printf("Number of blocks that are for writing: %d\n", BF_GetBlockCounter(fileDesc)-val-2);

    // Query B
    int maxim = -1, minim = -1, block_size, i, block_pos, all_records = 0;
    int seg_num, offset;
    void *block;
    
    for(i=0; i<(1<<depth); i++)
    {
        seg_num = (i*sizeof(int)) / BLOCK_SIZE; // number of segment that contains the ith bucket
        offset = (i*sizeof(int)) % BLOCK_SIZE; // position where the ith bucket is located in the segment

        if(BF_ReadBlock(fileDesc, 0, &block) < 0)
            return -1;

        memcpy(&block_pos, block+sizeof(char)+sizeof(EH_info)+sizeof(int)*seg_num, sizeof(int)); // retrieve block number where contains the ith bucket

        if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
            return -1;

        memcpy(&block_pos, block+offset, sizeof(int)); // retrieve block that is connected with the ith bucket
        
        if(!block_pos)
            continue;

        if(BF_ReadBlock(fileDesc, block_pos, &block) < 0)
            return -1;
    
        memcpy(&block_size, block, sizeof(int));
        all_records += block_size;
        
        minim = (minim == -1 ? block_size : min(minim, block_size));
        maxim = max(maxim, block_size);
    }
    
    printf("Minimum records in a bucket: %d\n", minim);
    printf("Maximum records in a bucket: %d\n", maxim);
    printf("Average records in a bucket: %.2f\n", (double)all_records/(double)(1<<depth));
    
    return 0;
}
